// import { useState } from "react";

// // @mui/material components
// import Card from "@mui/material/Card";
// import Switch from "@mui/material/Switch";
// import Checkbox from "@mui/material/Checkbox";
// import FormControlLabel from "@mui/material/FormControlLabel";

// // Soft UI Dashboard PRO components
// import SoftBox from "components/SoftBox";
// import SoftTypography from "components/SoftTypography";

// function Questions() {
//   const [followsMe, setFollowsMe] = useState(true);
//   const [answersPost, setAnswersPost] = useState(false);
//   const [mentionsMe, setMentionsMe] = useState(true);
//   const [newLaunches, setNewLaunches] = useState(false);
//   const [productUpdate, setProductUpdate] = useState(true);
//   const [newsletter, setNewsletter] = useState(true);

//   // State for the new questions
//   const [questions, setQuestions] = useState(Array(10).fill(false));

//   const handleQuestionChange = (index) => {
//     const newQuestions = [...questions];
//     newQuestions[index] = !newQuestions[index];
//     setQuestions(newQuestions);
//   };

//   const questionTexts = [
//     "What is React?",
//     "What is the history behind React evolution?",
//     "What are the major features of React?",
//     "What is JSX?",
//     "Difference between Element and Component?",
//     "How to create components in React?",
//     "When to use a Class Component?",
//     "What are Pure Components?",
//     "What is state in React?",
//     "What are props in React?",
//   ];

//   return (
//     <Card>
//       <SoftBox pt={2} px={2}>
//         <SoftBox mt={3}>
//           <SoftTypography
//             variant="caption"
//             fontWeight="bold"
//             color="text"
//             textTransform="uppercase"
//           >
//             Advanced
//           </SoftTypography>
//         </SoftBox>
//         <SoftBox
//           style={{ maxHeight: "420px", overflowY: "auto" }} // Set a max height and enable scroll
//         >
//           {questionTexts.map((questionText, index) => (
//             <SoftBox display="flex" py={1} mb={0.25} key={index}>
//               <SoftBox mt={0.25}>
//                 <Checkbox checked={questions[index]} onChange={() => handleQuestionChange(index)} />
//               </SoftBox>
//               <SoftBox width="80%" ml={2}>
//                 <SoftTypography variant="button" fontWeight="regular" color="text">
//                   {questionText}
//                 </SoftTypography>
//               </SoftBox>
//             </SoftBox>
//           ))}
//         </SoftBox>
//       </SoftBox>
//     </Card>
//   );
// }
const Questions = [
  "What is React?",
  "What is the history behind React evolution?",
  "What are the major features of React?",
  "What is JSX?",
  "Difference between Element and Component?",
  "How to create components in React?",
  "When to use a Class Component?",
  "What are Pure Components?",
  "What is state in React?",
  "What are props in React?",
];

export default Questions;
