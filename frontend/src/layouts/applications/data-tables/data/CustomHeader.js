// CustomHeader.js
import React from "react";
import PropTypes from "prop-types";

const CustomHeader = ({ title }) => (
  <th style={{ backgroundColor: "#4CAF50", color: "white", padding: "10px" }}>{title}</th>
);

CustomHeader.propTypes = {
  title: PropTypes.string.isRequired,
};

export default CustomHeader;
