import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Card from "@mui/material/Card";
import Switch from "@mui/material/Switch";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftInput from "components/SoftInput";
import SoftButton from "components/SoftButton";
import BasicLayout from "layouts/authentication/components/BasicLayout";
import Socials from "layouts/authentication/components/Socials";
import Separator from "layouts/authentication/components/Separator";
import curved9 from "assets/images/curved-images/curved9.jpg";
import axios from "axios";
import { useDispatch } from "react-redux";
import { setUser } from "actions/useActions";

function Basic() {
  const [rememberMe, setRememberMe] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errors, setErrors] = useState({ email: "", password: "", phoneNumber: "" });
  const navigate = useNavigate();
  const handleSetRememberMe = () => setRememberMe(!rememberMe);

  const validateEmail = (email) => {
    const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return re.test(String(email).toLowerCase());
  };

  const validatePassword = (password) => {
    const re = /^(?=.*).{6,}$/;
    return re.test(password);
  };
  const dispatch = useDispatch();

  const handleSubmit = () => {
    const emailError = validateEmail(email) ? "" : "Invalid email address";
    const passwordError = validatePassword(password) ? "" : "Invalid Password";
   
    setErrors({ email: emailError, password: passwordError });
   
    if (!emailError && !passwordError) {
      axios
        .get("http://localhost:5000/api/user/login", {
          params: { email, password }
        })
        .then((response) => {
          const { name, email } = response.data;
          dispatch(setUser(name, email)); // Dispatch the setUser action with name and email
          navigate("/dashboards");
        })
        .catch((error) => {
          console.error("There was an error!", error);
          setErrors({ ...errors, general: "Invalid email or password" });
        });
    }
  };
  return (
    <BasicLayout
      title="Welcome!"
      description="Use these awesome forms to login or create new account in your project for free."
      image={curved9}
    >
      <Card>
        <SoftBox p={3} mb={1} textAlign="center">
          <SoftTypography variant="h5" fontWeight="medium">
            Sign in
          </SoftTypography>
        </SoftBox>
        <SoftBox mb={2}>
          <Socials />
        </SoftBox>
        <SoftBox p={3}>
          <SoftBox component="form" role="form">
            <SoftBox mb={2}>
              <SoftInput
                type="email"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              {errors.email && (
                <SoftTypography color="error" variant="caption">
                  {errors.email}
                </SoftTypography>
              )}
            </SoftBox>
            <SoftBox mb={2}>
              <SoftInput
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              {errors.password && (
                <SoftTypography color="error" variant="caption">
                  {errors.password}
                </SoftTypography>
              )}
            </SoftBox>
            <SoftBox display="flex" alignItems="center">
              <Switch checked={rememberMe} onChange={handleSetRememberMe} />
              <SoftTypography
                variant="button"
                fontWeight="regular"
                onClick={handleSetRememberMe}
                sx={{ cursor: "pointer", userSelect: "none" }}
              >
                &nbsp;&nbsp;Remember me
              </SoftTypography>
            </SoftBox>
            <SoftBox mt={4} mb={1}>
              <SoftButton variant="gradient" color="info" fullWidth onClick={handleSubmit}>
                sign in
              </SoftButton>
            </SoftBox>
            {errors.general && (
              <SoftTypography color="error" variant="caption" textAlign="center">
                {errors.general}
              </SoftTypography>
            )}
            <Separator />
            <SoftBox mt={1} mb={3}>
              <SoftButton component={Link} to="/sign-up" variant="gradient" color="dark" fullWidth>
                sign up
              </SoftButton>
            </SoftBox>
          </SoftBox>
        </SoftBox>
      </Card>
    </BasicLayout>
  );
}

export default Basic;
