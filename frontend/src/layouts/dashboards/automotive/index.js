/**
=========================================================
* Soft UI Dashboard PRO- v4.0.2
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Grid from "@mui/material/Grid";

// Soft UI Dashboard PROcomponents

import VerifiedIcon from '@mui/icons-material/Verified';
import SoftBox from "components/SoftBox";
import PersonOffIcon from '@mui/icons-material/PersonOff';
import GroupIcon from '@mui/icons-material/Group';
// Soft UI Dashboard PROexample components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import MiniStatisticsCard from "examples/Cards/StatisticsCards/MiniStatisticsCard";
import defaultDoughnutChartData from "layouts/pages/charts/data/defaultDoughnutChartData";
import pieChartData from "layouts/pages/charts/data/pieChartData";
// Automotive dashboard components
// import AutomotiveDetails from "layouts/dashboards/automotive/components/AutomotiveDetails";
import AutomotiveMonitor from "layouts/dashboards/automotive/components/AutomotiveMonitor";
import DefaultDoughnutChart from "examples/Charts/DoughnutCharts/DefaultDoughnutChart";
import { PieChart } from "@mui/icons-material";
import FunnelChart from "layouts/pages/charts/data/FunnelChart";
import DualChart from "layouts/pages/charts/data/FunnelChart";

function Automotive() {
  return (
    <DashboardLayout>
      <DashboardNavbar />
      <SoftBox pt={3}>
        {/* <SoftBox mb={3}>
          <AutomotiveDetails />
        </SoftBox> */}
        <SoftBox mb={3}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={6} lg={3}>
              <MiniStatisticsCard
                bgColor="white"
                title={{ text: "Total Jobs", fontWeight: "medium",color:"black" }}
                count="100"
                icon={{ color: "info",component: "person" }}
              />
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
              <MiniStatisticsCard
                bgColor="white"
                title={{ text: "Total Candidates", fontWeight: "medium", color:"black" }}
                count="90"
                icon={{ color: "warning",component: <GroupIcon/> }}
              />
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
              <MiniStatisticsCard
                bgColor="white"
                title={{ text: "Selected Candidates", fontWeight: "medium",color:"black" }}
                count="70"
                icon={{ color: "success",component: <VerifiedIcon/> }}
              />
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
              <MiniStatisticsCard
                bgColor="white"
                title={{ text: "Rejected Candidates", fontWeight: "medium", color:"black" }}
                count="20"
                icon={{ color: "error", component: <PersonOffIcon /> }}
              />
            </Grid>
          </Grid>
        </SoftBox>
        <SoftBox mb={3}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={12}>
            <DualChart />
            </Grid>
            {/* <Grid item xs={12} md={6}>
            <FunnelChart />
            </Grid> */}
          </Grid>
        </SoftBox>
        {/* <SoftBox mb={3}>
          <AutomotiveMonitor />
        </SoftBox> */}
      </SoftBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Automotive;
