/**
=========================================================
* Soft UI Dashboard PRO- v4.0.2
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

/* eslint-disable react/prop-types */
// ProductsList page components
import IdCell from "layouts/ecommerce/orders/order-list/components/IdCell";
import DefaultCell from "layouts/ecommerce/orders/order-list/components/DefaultCell";
import StatusCell from "layouts/ecommerce/orders/order-list/components/StatusCell";
import CustomerCell from "layouts/ecommerce/orders/order-list/components/CustomerCell";

// Images
import team1 from "assets/images/team-1.jpg";
import team2 from "assets/images/team-2.jpg";
import team3 from "assets/images/team-3.jpg";
import team4 from "assets/images/team-4.jpg";
import team5 from "assets/images/team-5.jpg";
import ivana from "assets/images/ivana-squares.jpg";
import EditIcon from "@mui/icons-material/Edit"; 
const dataTableData = {
  columns: [
    { Header: "name", accessor: "name", Cell: ({ value }) => <IdCell id={value} /> },
    {
      Header: "Role",
      accessor: "role",
      Cell: ({ value }) => <DefaultCell value={value} />,
    },
    // {
    //   Header: "Email",
    //   accessor: "email",
    //   Cell: ({ value }) => {
    //     let status;

    //     if (value === "paid") {
    //       status = <StatusCell icon="done" color="success" status="Paid" />;
    //     } else if (value === "refunded") {
    //       status = <StatusCell icon="replay" color="dark" status="Refunded" />;
    //     } else {
    //       status = <StatusCell icon="close" color="error" status="Canceled" />;
    //     }

    //     return status;
    //   },
    // },
    { Header: "Email", accessor: "email", Cell: ({ value }) => <DefaultCell value={value} />},
    { Header: "Phone Number", accessor: "phoneNumber", Cell: ({ value }) => <DefaultCell value={value} />},
    
    // {
    //   Header: "Phone Number",
    //   accessor: "phoneNumber",
    //   Cell: ({ value: [name, data] }) => (
    //     <CustomerCell image={data.image} color={data.color || "dark"} name={name} />
    //   ),
    // },
    {
      Header: "Account Status",
      accessor: "accountStatus",
      Cell: ({ value }) => {
        const [name, data] = value;

        return (
          <DefaultCell
            value={typeof value === "string" ? value : name}
            suffix={data.suffix || false}
          />
        );
      },
    },
    // { Header: "Last Login ", accessor: "lastLogin ", Cell: ({ value }) => <DefaultCell value={value} /> },
    { Header: "Last Login", accessor: "lastLogin", Cell: ({ value }) => <DefaultCell value={value} />},
    { Header: "Actions", accessor: "actions", Cell: ({ value }) => <DefaultCell value={value} />},
  ],

  rows: [
    {
      name: "Star Bala",
      role: "Admin",
      email: "cbmounika55@gmail.com",
      phoneNumber: "9807654321",
      accountStatus: "Active",
      lastLogin : "20 jan 24, wed ",
      actions:<EditIcon sx={{width:"1.5em", height:"1.5em"}}/>,
    },
    {
      name: "Vadivel",
      role: "User",
      email: "tharunkumar13072@gmail.com",
      phoneNumber: "9876543210",
      accountStatus: "In Active",
      lastLogin: "22 may 24, wed ",
      actions:<EditIcon sx={{width:"1.5em", height:"1.5em"}}/>,
    },
    // {
    //   name: "#10423",
    //   role: "1 Nov, 11:13 AM",
    //   email: "refunded",
    //   phoneNumber: ["Michael Mirra", { image: "M" }],
    //   accountStatus: ["Leather Wallet", { suffix: "+1 more" }],
    //   lastLogin: "$25,50",
    //   actions:<EditIcon sx={{width:"1.5em", height:"1.5em"}}/>,
    // },
    // {
    //   name: "#10424",
    //   role: "1 Nov, 12:20 PM",
    //   email: "paid",
    //   phoneNumber: ["Andrew Nichel", { image: team3 }],
    //   accountStatus: "Bracelet Onu-Lino",
    //   lastLogin: "$19,40",
    //   actions:<EditIcon sx={{width:"1.5em", height:"1.5em"}}/>,
    // },
    // {
    //   name: "#10425",
    //   role: "1 Nov, 1:40 PM",
    //   email: "canceled",
    //   phoneNumber: ["Sebastian Koga", { image: team4 }],
    //   accountStatus: ["Phone Case Pink", { suffix: "x 2" }],
    //   lastLogin: "$44,90",
    //   actions:<EditIcon sx={{width:"1.5em", height:"1.5em"}}/>,
    // },
    // {
    //   name: "#10426",
    //   role: "1 Nov, 2:19 PM",
    //   email: "paid",
    //   phoneNumber: ["Laur Gilbert", { image: "L" }],
    //   accountStatus: "Backpack Niver",
    //   lastLogin: "$112,50",
    //   actions:<EditIcon sx={{width:"1.5em", height:"1.5em"}}/>,
    // },
    // {
    //   id: "#10427",
    //   date: "1 Nov, 3:42 AM",
    //   status: "paid",
    //   customer: ["Iryna Innda", { image: "I" }],
    //   product: "Adidas Vio",
    //   revenue: "$200,00",
    // },
    // {
    //   id: "#10428",
    //   date: "2 Nov, 9:32 AM",
    //   status: "paid",
    //   customer: ["Arrias Liunda", { image: "A" }],
    //   product: "Airpods 2 Gen",
    //   revenue: "$350,00",
    // },
    // {
    //   id: "#10429",
    //   date: "2 Nov, 10:14 AM",
    //   status: "paid",
    //   customer: ["Rugna Ilpio", { image: team5 }],
    //   product: "Bracelet Warret",
    //   revenue: "$15,00",
    // },
    // {
    //   id: "#10430",
    //   date: "2 Nov, 10:14 AM",
    //   status: "refunded",
    //   customer: ["Anna Landa", { image: ivana }],
    //   product: ["Watter Bottle India", { suffix: "x 3" }],
    //   revenue: "$25,00",
    // },
    // {
    //   id: "#10431",
    //   date: "2 Nov, 3:12 PM",
    //   status: "paid",
    //   customer: ["Karl Innas", { image: "K" }],
    //   product: "Kitchen Gadgets",
    //   revenue: "$164,90",
    // },
    // {
    //   id: "#10432",
    //   date: "2 Nov, 5:12 PM",
    //   status: "paid",
    //   customer: ["Oana Kilas", { image: "O", color: "info" }],
    //   product: "Office Papers",
    //   revenue: "$23,90",
    // },
  ],
};

export default dataTableData;
