/* eslint-disable react/prop-types */
// Soft UI Dashboard PROcomponents
// import SoftBadge from "components/SoftBadge";

// ProductsList page components
import ProductCell from "layouts/ecommerce/products/products-list/components/ProductCell";
// import ActionCell from "layouts/ecommerce/products/products-list/components/ActionCell";

// Images
import macBookPro from "assets/images/ecommerce/macbook-pro.jpeg";
import CompanyTableIcon from "../components/ActionCell/CompanyTableIcon";

// Badges
// const outOfStock = (
//   <SoftBadge variant="contained" color="error" size="xs" badgeContent="inactive" container />
// );
// const inStock = (
//   <SoftBadge variant="contained" color="success" size="xs" badgeContent="active" container />
// );

const BankInformationData = {
  columns: [
    {
      Header: "Account Type",
      accessor: "accountType",
      width: "17%",
      Cell: ({ value }) => {
        if (Array.isArray(value)) {
          const [name, data] = value;
          return <ProductCell image={data.image} name={name} checked={data.checked} />;
        }
        return null; // or handle this case appropriately
      },
    },
    { Header: "Bank Institution Name", accessor: "BankInstitutionName", width: "15%" },
    { Header: "Account Holder Name", accessor: "accountHolderName", width: "15%" },
    { Header: "Branch Name", accessor: "branchName", width: "15%" },
    { Header: "Branch Code", accessor: "branchCode", width: "15%" },
    { Header: "Action", accessor: "action" },
  ],

  rows: [
    {
      accountType: ["Account Type", { image: macBookPro, checked: true }],
      BankInstitutionName: "Bank Institution Name",
      accountHolderName: "Account Holder Name",
      branchName: "Branch Name",
      branchCode: "Branch Code",
      action: <CompanyTableIcon />,
    },
  ],
};

export default BankInformationData;
