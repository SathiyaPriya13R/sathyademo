/**
=========================================================
* Soft UI Dashboard PRO- v4.0.2
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// react-router-dom components
// import { Link } from "react-router-dom";
import React from "react";

// @mui material components
import Card from "@mui/material/Card";
// import Stack from "@mui/material/Stack";

// Soft UI Dashboard PROcomponents
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftButton from "components/SoftButton";

// Soft UI Dashboard PROexample components
// import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
// import DashboardNavbar from "examples/Navbars/DashboardNavbar";
// import Footer from "examples/Footer";
import DataTable from "examples/Tables/DataTable";
// import { Autocomplete, Button, Dialog, DialogActions, DialogContent, DialogTitle, Icon, IconButton, Menu, MenuItem, TextField, Tooltip } from '@mui/material';

// Data
import BankInformationData from "./BackInformationData";

function BackInformationList() {
  return (
    <>
      <SoftBox my={3}>
        <Card>
          <SoftTypography
            variant="h6"
            mb={2}
            sx={{ marginLeft: "30px", marginTop: "30px", marginBottom: "30px" }}
          >
            Bank Information
          </SoftTypography>
          <DataTable
            table={BankInformationData}
            entriesPerPage={{
              defaultValue: 7,
              entries: [5, 7, 10, 15, 20, 25],
            }}
            canSearch
          />
          <SoftBox my={3}>
            <SoftButton variant="text" color="info">
              + Add New
            </SoftButton>
          </SoftBox>
        </Card>
      </SoftBox>
    </>
  );
}

export default BackInformationList;
