/**
=========================================================
* Soft UI Dashboard PRO- v4.0.2
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// react-router-dom components
import { Link } from "react-router-dom";
import React, { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
// @mui material components
import Card from "@mui/material/Card";
import Stack from "@mui/material/Stack";

// Soft UI Dashboard PROcomponents
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftButton from "components/SoftButton";

// Soft UI Dashboard PROexample components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import DataTable from "examples/Tables/DataTable";
import { Autocomplete, Button, Dialog, DialogActions, DialogContent, DialogTitle, Icon, IconButton, InputAdornment, Menu, MenuItem, TextField, Tooltip } from '@mui/material';

// Data
import ActiveDataTable from "./activeDataTable";
import { FilterList, Search } from "@mui/icons-material";
import JobTable from "examples/Tables/DataTable/jobTable";
import SoftInputDateTime from "./SoftInputDateTime";

function ActiveList() {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const [open, setOpen] = useState(false);
  const [field1, setField1] = useState('');
  const [field2, setField2] = useState('');
  const [field3, setField3] = useState('');

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose1 = () => {
    setOpen(false);
  };
  const [startDate, setStartDate] = useState(null);

  const top100Films = [
    { label: 'ABCD542S', year: 1994 },
    { label: 'ABCD562S', year: 1972 },
    { label: 'ABCD5424D', year: 1974 },

  ];
  const clientdrop = [
    { label: 'Bala', year: 1994 },
    { label: 'Vadivel', year: 1972 },
    { label: 'Prabha', year: 1974 },

  ];
  const statusdropdown = [
    { label: 'Active', year: 1994 },
    { label: 'In Active', year: 1972 },
    { label: 'Completed', year: 1974 },

  ];
  const handleApply = () => {
    // Handle the apply logic here
    console.log('Field 1:', field1);
    console.log('Field 2:', field2);
    console.log('Field 3:', field3);
    setOpen(false);
  };
  return (
    <DashboardLayout>
      <DashboardNavbar />
      <SoftBox my={3}>
        <Card>
          <SoftBox display="flex" justifyContent="space-between" alignItems="flex-start" p={3}>
          {/* <SoftBox display="flex" alignItems="center" lineHeight={1}>
      <SoftTypography variant="h5" fontWeight="medium">
        Jobs
      </SoftTypography> */}
      <SoftBox display="flex" alignItems="center" lineHeight={1}>
              <SoftTypography variant="h5" fontWeight="medium">
                Jobs
              </SoftTypography>
              <SoftBox ml={1}>
                <Tooltip>
                  <IconButton onClick={handleClick}>
                    <Icon>arrow_drop_down</Icon>
                  </IconButton>
                </Tooltip>
                <Menu
                  anchorEl={anchorEl}
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                >

                  <MenuItem onClick={handleClose}>Active</MenuItem>
                  <MenuItem onClick={handleClose}>Inactive</MenuItem>
                  <MenuItem onClick={handleClose}>Completed</MenuItem>
                </Menu>
              </SoftBox>
            </SoftBox>
    {/* </SoftBox> */}
            <Stack spacing={1} direction="row">
            <TextField
                  variant="outlined"
                  size="small"
                  placeholder="Search"
                
                //   value={search}
                //   onChange={(e) => setSearch(e.target.value)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <IconButton>
                          <Search />
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  sx={{
                    ".css-15v2jta-MuiInputBase-root-MuiOutlinedInput-root": {
                      height: "46px !important",
                    },
                    marginRight: 1,
                  }}
                />
              {/* <SoftButton variant="outlined" color="info" size="small"  startIcon={<FilterList />}>
                Filter
              </SoftButton> */}
              <div>
   <SoftButton variant="gradient" color="info" size="small" sx={{marginRight: 1 , height:"46px"}} startIcon={<FilterList />} onClick={handleClickOpen}>
                Filter
              </SoftButton>
              <Dialog open={open} onClose={handleClose1} maxWidth="md">
        {/* <DialogTitle>Filter</DialogTitle> */}
        <DialogContent sx={{ width: 500 }}>
          <div style={{ display: 'flex', flexDirection: 'column', gap: '20px',padding:"47px" }}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px',fontSize:"1rem" }}>Date Range</SoftTypography>
              <SoftInputDateTime
                  // label="Start Date"
                  value={startDate}
                  onChange={(e) => setStartDate(e.target.value)}
                  sx={{width:"500px !important" }}
                />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px',fontSize:"1rem"  }}>Job ID</SoftTypography>
              <Autocomplete
                disablePortal
                id="name-autocomplete"
                options={top100Films}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Job ID"/>}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px',fontSize:"1rem"  }}>Client</SoftTypography>
              <Autocomplete
                disablePortal
                id="age-autocomplete"
                options={clientdrop}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Client"/>}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px',fontSize:"1rem"  }}>Status</SoftTypography>
              <Autocomplete
                disablePortal
                id="location-autocomplete"
                options={statusdropdown}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Status" />}
              />
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <SoftButton variant="gradient" color="info" onClick={handleClose1} >
            Reset
          </SoftButton>
          <SoftButton variant="gradient" color="info" onClick={handleApply} >
            Apply
          </SoftButton>
        </DialogActions>
      </Dialog>

    </div>
              {/* <SoftButton variant="outlined" color="info" size="small">
                export
              </SoftButton> */}
              {/* <Link to="/ecommerce/products/new-product">
                <SoftButton variant="gradient" color="info" size="small">
                  + Add Resume
                </SoftButton>
              </Link> */}
            </Stack>
          </SoftBox>
          <JobTable
            table={ActiveDataTable}
            entriesPerPage={{
              defaultValue: 7,
              entries: [5, 7, 10, 15, 20, 25],
            }}
            canSearch
          />
        </Card>
      </SoftBox>
      <Footer />
    </DashboardLayout>
  );
}

export default ActiveList;
