/**
=========================================================
* Soft UI Dashboard PRO- v4.0.2
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// react-router-dom components
import { Link } from "react-router-dom";
import React, { useState } from "react";

// @mui material components
import Card from "@mui/material/Card";
import Stack from "@mui/material/Stack";

// Soft UI Dashboard PROcomponents
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftButton from "components/SoftButton";

// Soft UI Dashboard PROexample components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import DataTable from "examples/Tables/DataTable";
import { Autocomplete, Button, Dialog, DialogActions, DialogContent, DialogTitle, Icon, IconButton, Menu, MenuItem, TextField, Tooltip } from '@mui/material';

// Data
import ClientDataTable from "./clientDataTable";
import { FilterList } from "@mui/icons-material";
import SoftInputDateTime from "../activeJobs/SoftInputDateTime";

function ClientList() {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const [open, setOpen] = useState(false);
  const [field1, setField1] = useState('');
  const [field2, setField2] = useState('');
  const [field3, setField3] = useState('');
  const [startDate, setStartDate] = useState(null);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose1 = () => {
    setOpen(false);
  };
  

  const top100Films = [
    { label: 'Star Bala', year: 1994 },
    { label: 'Vadivel', year: 1972 },
    { label: 'Prabha', year: 1974 },
    { label: 'Mani', year: 2008 },
    { label: 'Subash', year: 1957 },
    { label: "Siva", year: 1993 },
    { label: 'Veeranmani', year: 1994 },
   
  ];
  const MobileNo = [
    { label: '9876543210', year: 1994 },
    { label: '9876543210', year: 1972 },
    { label: '9876543210', year: 1974 },
    { label: '9876543210', year: 2008 },
    { label: '9876543210', year: 1957 },
    { label: "9876543210", year: 1993 },
    { label: '9876543210', year: 1994 },
   
  ];
  const statusdrop = [
    { label: 'Active', year: 1994 },
    { label: 'In Active', year: 1972 },
    { label: 'completed', year: 1974 },
 
   
  ];
  const contactPerson = [
    { label: 'bala', year: 1994 },
    { label: 'anand', year: 1972 },
    { label: 'praveen', year: 1974 },
    { label: 'riyas', year: 2008 },
    { label: 'tharun', year: 1957 },
    { label: "yuga", year: 1993 },
    { label: 'k7', year: 1994 },
   
  ];
  
  const handleApply = () => {
    // Handle the apply logic here
    console.log('Field 1:', field1);
    console.log('Field 2:', field2);
    console.log('Field 3:', field3);
    setOpen(false);
  };
  return (
    <DashboardLayout>
      <DashboardNavbar />
      <SoftBox my={3}>
        <Card>
          <SoftBox display="flex" justifyContent="space-between" alignItems="flex-start" p={3}>
          {/* <SoftBox display="flex" alignItems="center" lineHeight={1}>
      <SoftTypography variant="h5" fontWeight="medium">
        Clients
      </SoftTypography>
      
    </SoftBox> */}
    <SoftBox display="flex" alignItems="center" lineHeight={1}>
              <SoftTypography variant="h5" fontWeight="medium">
              Clients
              </SoftTypography>
              <SoftBox ml={1}>
                <Tooltip>
                  <IconButton onClick={handleClick}>
                    <Icon>arrow_drop_down</Icon>
                  </IconButton>
                </Tooltip>
                <Menu
                  anchorEl={anchorEl}
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                >

                  <MenuItem onClick={handleClose}>Active</MenuItem>
                  <MenuItem onClick={handleClose}>Inactive</MenuItem>
                  <MenuItem onClick={handleClose}>Completed</MenuItem>
                </Menu>
              </SoftBox>
            </SoftBox>
            <Stack spacing={1} direction="row">
              
              {/* <SoftButton variant="outlined" color="info" size="small" sx={{height:"46px"}} startIcon={<FilterList />}>
                Filter
              </SoftButton> */}
                <div>
   <SoftButton variant="gradient" color="info" size="small" sx={{marginRight: 1 , height:"46px"}} startIcon={<FilterList />} onClick={handleClickOpen}>
                Filter
              </SoftButton>
              <Dialog open={open} onClose={handleClose1} maxWidth="md">
        {/* <DialogTitle>Filter</DialogTitle> */}
        <DialogContent sx={{ width: 500 }}>
          <div style={{ display: 'flex', flexDirection: 'column', gap: '20px' , padding:"47px"}}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px',fontSize:"1rem" }}>Date Range</SoftTypography>
              <SoftInputDateTime
                  // label="Start Date"
                  value={startDate}
                  onChange={(e) => setStartDate(e.target.value)}
                  sx={{width:"500px !important" }}
                />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px' ,fontSize:"1rem" }}>Name</SoftTypography>
              <Autocomplete
                disablePortal
                id="name-autocomplete"
                options={top100Films}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Name"/>}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px',fontSize:"1rem"  }}>Mobile No</SoftTypography>
              <Autocomplete
                disablePortal
                id="name-autocomplete"
                options={MobileNo}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Mobile No"/>}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px',fontSize:"1rem"  }}>Contact Person</SoftTypography>
              <Autocomplete
                disablePortal
                id="age-autocomplete"
                options={contactPerson}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Contact Person"/>}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px',fontSize:"1rem"  }}>Status</SoftTypography>
              <Autocomplete
                disablePortal
                id="location-autocomplete"
                options={statusdrop}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Status" />}
              />
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <SoftButton variant="gradient" color="info" onClick={handleClose1} >
            Reset
          </SoftButton>
          <SoftButton variant="gradient" color="info" onClick={handleApply} >
            Apply
          </SoftButton>
        </DialogActions>
      </Dialog>

    </div>
              {/* <SoftButton variant="outlined" color="info" size="small">
                export
              </SoftButton> */}
              <Link to="/agencyProfile">
                <SoftButton variant="gradient" color="info" size="small" sx={{height:"46px"}}>
                  + Add Client
                </SoftButton>
              </Link>
            </Stack>
          </SoftBox>
          <DataTable
            table={ClientDataTable}
            entriesPerPage={{
              defaultValue: 7,
              entries: [5, 7, 10, 15, 20, 25],
            }}
            canSearch
          />
        </Card>
      </SoftBox>
      <Footer />
    </DashboardLayout>
  );
}

export default ClientList;
