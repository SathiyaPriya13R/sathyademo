/**
=========================================================
* Soft UI Dashboard PRO- v4.0.2
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// */

// // @mui material components
// import Icon from "@mui/material/Icon";
// import Tooltip from "@mui/material/Tooltip";

// // Soft UI Dashboard PROcomponents
// import SoftBox from "components/SoftBox";
// import SoftTypography from "components/SoftTypography";

// function DownloadIcon() {
//   return (
//     <SoftBox display="flex" alignItems="center">
//       <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }}>
//         <Tooltip title="Preview product" placement="top">
//           <Icon>visibility</Icon>
//         </Tooltip>
//       </SoftTypography>
//       <SoftBox mx={2}>
//         <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }}>
//           <Tooltip title="Edit product" placement="top">
//             <Icon>edit</Icon>
//           </Tooltip>
//         </SoftTypography>
//       </SoftBox>
//       <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }}>
//         <Tooltip title="Delete product" placement="left">
//           <Icon>delete</Icon>
//         </Tooltip>
//       </SoftTypography>
//     </SoftBox>
//   );
// }

// export default DownloadIcon;


// // @mui material components
// import Icon from "@mui/material/Icon";
// import Tooltip from "@mui/material/Tooltip";

// // Soft UI Dashboard PRO components
// import SoftBox from "components/SoftBox";
// import SoftTypography from "components/SoftTypography";

// function DownloadIcon() {
//   return (
//     <SoftBox display="flex" alignItems="center">
//          <SoftBox mx={2}>
//         <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }}>
//           <Tooltip title="View video" placement="top">
//             <Icon>videocam</Icon>
//           </Tooltip>
//         </SoftTypography>
//       </SoftBox>
//       <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }}>
//         <Tooltip title="Download " placement="top">
//           <Icon>download</Icon>
//         </Tooltip>
//       </SoftTypography>
     
//     </SoftBox>
//   );
// }

// export default DownloadIcon;


import React, { useState } from "react";
// @mui material components
import Icon from "@mui/material/Icon";
import Tooltip from "@mui/material/Tooltip";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";

// Soft UI Dashboard PRO components
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";

// Modal style
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '80%',
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
};

function DownloadIcon() {
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      <SoftBox display="flex" alignItems="center">
        <SoftBox mx={2}>
          <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }} onClick={handleOpen}>
            <Tooltip title="View video" placement="top">
              <Icon>videocam</Icon>
            </Tooltip>
          </SoftTypography>
        </SoftBox>
        <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }}>
          <Tooltip title="Download" placement="top">
            <Icon>download</Icon>
          </Tooltip>
        </SoftTypography>
      </SoftBox>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-video-title"
        aria-describedby="modal-video-description"
      >
        <Box sx={style}>
          <SoftTypography id="modal-video-title" variant="h6" component="h2">
            Product Video
          </SoftTypography>
          <SoftBox id="modal-video-description" sx={{ mt: 2 }}>
            <video width="100%" controls>
              <source src="sample-video-url.mp4" type="video/mp4" />
              Your browser does not support the video tag.
            </video>
          </SoftBox>
        </Box>
      </Modal>
    </>
  );
}

export default DownloadIcon;
