// /**
// =========================================================
// * Soft UI Dashboard PRO- v4.0.2
// =========================================================

// * Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
// * Copyright 2023 Creative Tim (https://www.creative-tim.com)

// Coded by www.creative-tim.com

//  =========================================================

// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// */

// // @mui material components
// import Icon from "@mui/material/Icon";
// import Tooltip from "@mui/material/Tooltip";
// import React, { useState } from "react";
// // Soft UI Dashboard PROcomponents
// import SoftBox from "components/SoftBox";
// import SoftTypography from "components/SoftTypography";
// import EventAvailableIcons from '@mui/icons-material/EventAvailable';
// import { Link } from "react-router-dom";
// import PropTypes from 'prop-types';
// function EventAvailableIcon(props) {
//  // const[mailid,setMailId]=useState([]);
//   console.log(props.passedvalue,"jkl");
  
//   //setMailId(props.passedvalue);
// const arrayValue=props.passedvalue;
//   return (
//     <SoftBox display="flex" alignItems="center">
//        <Link to={{
//     pathname: "/newSchedule",
//     state: {arrayValue}
// }}>
//              <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }} >
//           <Tooltip title="Schedule" placement="top">
//      <EventAvailableIcons/>
//           </Tooltip>
//         </SoftTypography>
//         </Link>
//         <SoftBox mx={2}>
//         <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }}>
//           <Tooltip title="Edit" placement="top">
//             <Icon>edit</Icon>
//           </Tooltip>
//         </SoftTypography>
//       </SoftBox>
//       <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }}>
//         <Tooltip title="Preview " placement="top">
//           <Icon>visibility</Icon>
//         </Tooltip>
//       </SoftTypography>
//       <SoftBox mx={2}>
//       <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }}>
//         <Tooltip title="Delete" placement="left">
//           <Icon>delete</Icon>
//         </Tooltip>
//       </SoftTypography>
//       </SoftBox>
//     </SoftBox>
//   );
// }
// EventAvailableIcon.propTypes = {
//   passedvalue:PropTypes.string.isRequired
// }

// export default EventAvailableIcon;

import React from "react";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import Icon from "@mui/material/Icon";
import Tooltip from "@mui/material/Tooltip";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import EventAvailableIcons from "@mui/icons-material/EventAvailable";

function EventAvailableIcon({ passedvalue }) {
  const navigate = useNavigate();

  const handleNavigate = () => {
    navigate("/newSchedule", { state: { arrayValue: passedvalue } });
  };

  return (
  
      <SoftBox display="flex" alignItems="center">
          <div onClick={handleNavigate} style={{ cursor: "pointer" }}>
        <SoftTypography variant="body1" color="secondary" sx={{ lineHeight: 0 }}>
          <Tooltip title="Schedule" placement="top">
            <EventAvailableIcons />
          </Tooltip>
        </SoftTypography>
        </div>
        <SoftBox mx={2}>
          <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }}>
            <Tooltip title="Edit" placement="top">
              <Icon>edit</Icon>
            </Tooltip>
          </SoftTypography>
        </SoftBox>
        <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }}>
          <Tooltip title="Preview" placement="top">
            <Icon>visibility</Icon>
          </Tooltip>
        </SoftTypography>
        <SoftBox mx={2}>
          <SoftTypography variant="body1" color="secondary" sx={{ cursor: "pointer", lineHeight: 0 }}>
            <Tooltip title="Delete" placement="left">
              <Icon>delete</Icon>
            </Tooltip>
          </SoftTypography>
        </SoftBox>
      </SoftBox>
  );
}

EventAvailableIcon.propTypes = {
  passedvalue: PropTypes.string.isRequired
};

export default EventAvailableIcon;
