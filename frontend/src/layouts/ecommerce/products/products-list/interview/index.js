

import { Link } from "react-router-dom";
import React, { useState,useEffect } from "react";
import axios from 'axios';
// @mui material components
import Card from "@mui/material/Card";
import Stack from "@mui/material/Stack";

import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
// Soft UI Dashboard PROcomponents
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftButton from "components/SoftButton";

// Soft UI Dashboard PROexample components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import DataTable from "examples/Tables/DataTable";
import { Autocomplete, Button, Dialog, DialogActions, DialogContent, DialogTitle, Icon, IconButton, Menu, MenuItem, TextField, Tooltip } from '@mui/material';
import SoftBadge from "components/SoftBadge";
import ProductCell from "layouts/ecommerce/products/products-list/components/ProductCell";
import ActionCell from "layouts/ecommerce/products/products-list/components/ActionCell";
import macBookPro from "assets/images/ecommerce/macbook-pro.jpeg";
// Data
// import ClientDataTable from "./clientDataTable";
import { FilterList } from "@mui/icons-material";
import SoftInputDateTime from "../activeJobs/SoftInputDateTime";

function InterViewList() {
  const [schedules, setSchedules] = useState([]);
  const [error, setError] = useState(null);
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const [open, setOpen] = useState(false);
  const [field1, setField1] = useState('');
  const [field2, setField2] = useState('');
  const [field3, setField3] = useState('');
  const [startDate, setStartDate] = useState(null);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose1 = () => {
    setOpen(false);
  };
  const outOfStock = (
    <SoftBadge variant="contained" color="error" size="xs" badgeContent="inactive" container />
  );
  const inStock = (
    <SoftBadge variant="contained" color="success" size="xs" badgeContent="active" container />
  );
  const complete = (
    <SoftBadge variant="contained" color="warning" size="xs" badgeContent="complete" container />
  );
  useEffect(() => {
    // Fetch schedules from the API using axios
    axios.get('http://127.0.0.1:5000/api/schedule/get_schedule')
      .then(response => {
        console.log('Fetched schedules:', response.data);  // Log data to the console
        setSchedules(response.data);
      })
      .catch(error => {
        console.error('Error fetching schedules:', error);  // Log error to the console
        setError(error);
      });
  }, []);

  if (error) {
    return <div>Error: {error.message}</div>;
  }
  const result = schedules.map(({ candidate_email, CreatedDate,startDate,linkDate  }) => ({ candidate_email,CreatedDate,startDate,linkDate, action: <ActionCell />}));
  console.log(result);

  const InterViewDataTable = {
    columns: [
      {
        Header: "Candidate Email",
        accessor: "candidate_email",
        width: "17%",
  
      },

      {
        Header: "Start date",
        accessor: "startDate",
        width: "17%",
  
      },
      { Header: "End date", accessor: "linkDate", width: "15%" },
      { Header: "Created date", accessor: "CreatedDate", width: "15%" },
      {
        Header: "status",
        accessor: "status",
        // eslint-disable-next-line react/prop-types
        Cell: ({ value }) => {
          if (value === "active") return inStock;
          if (value === "complete") return complete;
          return outOfStock;
        },
      },
      { Header: "action", accessor: "action" },
    ],
  
    rows: result
  };
  
  const top100Films = [
    { label: 'Star Bala', year: 1994 },
    { label: 'Vadivel', year: 1972 },
    { label: 'Prabha', year: 1974 },
    { label: 'Mani', year: 2008 },
    { label: 'Subash', year: 1957 },
    { label: "Siva", year: 1993 },
    { label: 'Veeranmani', year: 1994 },
   
  ];
  const MobileNo = [
    { label: '9876543210', year: 1994 },
    { label: '9876543210', year: 1972 },
    { label: '9876543210', year: 1974 },
    { label: '9876543210', year: 2008 },
    { label: '9876543210', year: 1957 },
    { label: "9876543210", year: 1993 },
    { label: '9876543210', year: 1994 },
   
  ];
  const statusdrop = [
    { label: 'Active', year: 1994 },
    { label: 'In Active', year: 1972 },
    { label: 'completed', year: 1974 },
 
   
  ];
  const contactPerson = [
    { label: 'bala', year: 1994 },
    { label: 'anand', year: 1972 },
    { label: 'praveen', year: 1974 },
    { label: 'riyas', year: 2008 },
    { label: 'tharun', year: 1957 },
    { label: "yuga", year: 1993 },
    { label: 'k7', year: 1994 },
   
  ];
  
  const handleApply = () => {
    // Handle the apply logic here
    console.log('Field 1:', field1);
    console.log('Field 2:', field2);
    console.log('Field 3:', field3);
    setOpen(false);
  };


  return (
    <DashboardLayout>
      <DashboardNavbar />
      <SoftBox my={3}>
        <Card>
          <SoftBox display="flex" justifyContent="space-between" alignItems="flex-start" p={3}>
          {/* <SoftBox display="flex" alignItems="center" lineHeight={1}>
      <SoftTypography variant="h5" fontWeight="medium">
        Interview Schedule
      </SoftTypography>

      
    </SoftBox> */}
    <SoftBox display="flex" alignItems="center" lineHeight={1}>
              <SoftTypography variant="h5" fontWeight="medium">
              Interview Schedule
              </SoftTypography>
              <SoftBox ml={1}>
                <Tooltip>
                  <IconButton onClick={handleClick}>
                    <Icon>arrow_drop_down</Icon>
                  </IconButton>
                </Tooltip>
                <Menu
                  anchorEl={anchorEl}
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                >

                  <MenuItem onClick={handleClose}>Active</MenuItem>
                  <MenuItem onClick={handleClose}>Inactive</MenuItem>
                  <MenuItem onClick={handleClose}>Completed</MenuItem>
                </Menu>
              </SoftBox>
            </SoftBox>
            <Stack spacing={1} direction="row">
              
              {/* <SoftButton variant="outlined" color="info" size="small" sx={{height:"46px"}} startIcon={<FilterList />}>
                Filter
              </SoftButton> */}
                <div>
   <SoftButton variant="gradient" color="info" size="small" sx={{marginRight: 1 , height:"46px"}} startIcon={<FilterList />} onClick={handleClickOpen}>
                Filter
              </SoftButton>
              <Dialog open={open} onClose={handleClose1} maxWidth="md">
        {/* <DialogTitle>Filter</DialogTitle> */}
        <DialogContent sx={{ width: 500 }}>
          <div style={{ display: 'flex', flexDirection: 'column', gap: '20px' ,padding:"46px"}}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px',fontSize:"1rem" }}>Date Range</SoftTypography>
              <SoftInputDateTime
                  // label="Start Date"
                  value={startDate}
                  onChange={(e) => setStartDate(e.target.value)}
                  sx={{width:"500px !important" }}
                />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px',fontSize:"1rem" }}>Name</SoftTypography>
              <Autocomplete
                disablePortal
                id="name-autocomplete"
                options={top100Films}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Name"/>}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px',fontSize:"1rem" }}>Mobile No</SoftTypography>
              <Autocomplete
                disablePortal
                id="name-autocomplete"
                options={MobileNo}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Mobile No"/>}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px' ,fontSize:"1rem"}}>Contact Person</SoftTypography>
              <Autocomplete
                disablePortal
                id="age-autocomplete"
                options={contactPerson}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Contact Person"/>}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px',fontSize:"1rem" }}>Status</SoftTypography>
              <Autocomplete
                disablePortal
                id="location-autocomplete"
                options={statusdrop}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Status" />}
              />
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <SoftButton variant="gradient" color="info"  onClick={handleClose1}>
            Reset
          </SoftButton>
          <SoftButton variant="gradient" color="info"  onClick={handleApply}>
            Apply
          </SoftButton>
        </DialogActions>
      </Dialog>

    </div>
              {/* <SoftButton variant="outlined" color="info" size="small">
                export
              </SoftButton> */}
              <Link to="/newSchedule">
              <SoftButton variant="gradient" color="info" size="small" sx={{ height: "46px" }} startIcon={<CalendarMonthIcon />}>
  New Schedule
</SoftButton>
              </Link>
            </Stack>
          </SoftBox>
          <DataTable
            table={InterViewDataTable}
            entriesPerPage={{
              defaultValue: 7,
              entries: [5, 7, 10, 15, 20, 25],
            }}
            canSearch
          />
        </Card>
      </SoftBox>
      <Footer />
    </DashboardLayout>
  );
}

export default InterViewList;
