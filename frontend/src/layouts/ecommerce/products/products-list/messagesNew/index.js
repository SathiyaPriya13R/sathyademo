// Import PropTypes
import PropTypes from "prop-types";

// Soft UI Dashboard PRO components

// ProductsList page components

import ActionCell from "../components/ActionCell";
// import ProductCell from "layouts/ecommerce/products/products-list/components/ProductCell";
// import ActionCell from "layouts/ecommerce/products/products-list/components/ActionCell";

// Images


// Badges


const MessagesNew = {
  columns: [
    // {
    //   Header: "Start date",
    //   accessor: "startdate",
    //   width: "17%",
    //   // eslint-disable-next-line react/prop-types
    //   Cell: ({ value }) => {
    //     const [name, data] = value;
    //     return <ProductCell image={data.image} name={name} checked={data.checked} />;
    //   },
    // },
    { Header: "Subject", accessor: "subject", width: "30%" },
    { Header: "Contact name", accessor: "contactName", width: "20%" },
    { Header: "Vendor name", accessor: "vendorName", width: "25%" },
    { Header: "Message date", accessor: "messageDate", width: "25%" },
    // { Header: "Created date", accessor: "createdDate", width: "15%" },
    // {
    //   Header: "status",
    //   accessor: "status",
    //   // eslint-disable-next-line react/prop-types
    //   Cell: ({ value }) => {
    //     if (value === "active") return inStock;
    //     if (value === "complete") return complete;
    //     return outOfStock;
    //   },
    // },
    { Header: "action", accessor: "action" },
  ],

  rows: [
    {
      subject: "Lorem ipsum",
      contactName: "Santhosh Kumar",
      vendorName: "SmartEye Technologies",
      messageDate: "09 Jan 23, Wed",
      action: <ActionCell />,
    },
    {
      subject: "Lorem ipsum",
      contactName: "Thilak",
      vendorName: "Inkoop Technologies",
      messageDate: "09 Jan 23, Wed",
      action: <ActionCell />,
    },
    {
      subject: "Lorem ipsum",
      contactName: "Santhosh Kumar",
      vendorName: "SmartEye Technologies",
      messageDate: "09 may 24",
      action: <ActionCell />,
    },
  ],
};

// PropTypes validation for Cell components
const CellPropTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
};

// Apply PropTypes validation to Cell components
MessagesNew.columns.forEach((column) => {
  if (column.Cell) {
    column.Cell.propTypes = CellPropTypes;
  }
});

export default MessagesNew;
