import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Grid from "@mui/material/Grid";
import AppBar from "@mui/material/AppBar";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import SoftBox from "components/SoftBox";
import breakpoints from "assets/theme/base/breakpoints";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";

// Correct the import paths based on your project structure
import BasicInfo from "layouts/pages/account/security/tabs/BasicInfo";
import Business from "layouts/pages/account/security/tabs/Business";
import Marketing from "layouts/pages/account/security/tabs/Marketing";
import Contacts from "layouts/pages/account/security/tabs/Contacts";
import Certifications from "layouts/pages/account/security/tabs/Certifications";
import AdditionalDocuments from "layouts/pages/account/security/tabs/AdditionalDocuments";

function BaseLayout({ stickyNavbar, children }) {
  const [tabsOrientation, setTabsOrientation] = useState("horizontal");
  const [tabValue, setTabValue] = useState(0);

  useEffect(() => {
    function handleTabsOrientation() {
      return window.innerWidth < breakpoints.values.sm
        ? setTabsOrientation("vertical")
        : setTabsOrientation("horizontal");
    }

    window.addEventListener("resize", handleTabsOrientation);
    handleTabsOrientation();

    return () => window.removeEventListener("resize", handleTabsOrientation);
  }, [tabsOrientation]);

  const handleSetTabValue = (event, newValue) => setTabValue(newValue);

  const renderTabContent = () => {
    switch (tabValue) {
      case 0:
        return <BasicInfo />;
      case 1:
        return <Business />;
      case 2:
        return <Marketing />;
      case 3:
        return <Contacts />;
      case 4:
        return <Certifications />;
      case 5:
        return <AdditionalDocuments />;
      default:
        return null;
    }
  };

  return (
    <DashboardLayout>
      <DashboardNavbar absolute={!stickyNavbar} isMini />
      <SoftBox mt={stickyNavbar ? 3 : 10}>
        <Grid container>
          <Grid item xs={12} sm={12} lg={8}>
            <AppBar position="static">
              <Tabs orientation={tabsOrientation} value={tabValue} onChange={handleSetTabValue}>
                <Tab label="Basic Info" />
                <Tab label="Business" />
                <Tab label="Marketing" />
                <Tab label="Contacts" />
                <Tab label="Certifications" />
                <Tab label="Additional Documents" />
              </Tabs>
            </AppBar>
          </Grid>
        </Grid>
        {renderTabContent()}
      </SoftBox>
      <Footer />
    </DashboardLayout>
  );
}

BaseLayout.defaultProps = {
  stickyNavbar: false,
};

BaseLayout.propTypes = {
  stickyNavbar: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

export default BaseLayout;
