/**
=========================================================
* Soft UI Dashboard PRO- v4.0.2
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Grid from "@mui/material/Grid";
import Divider from "@mui/material/Divider";

// Soft UI Dashboard PROcomponents
import SoftBox from "components/SoftBox";
import SoftSelect from "components/SoftSelect";
import SoftTypography from "components/SoftTypography";
// Security page components
import BaseLayout from "layouts/pages/account/components/BaseLayout";
import FormField from "layouts/pages/account/components/FormField";
import { Card } from "@mui/material";
import CompanyAddressList from "layouts/ecommerce/products/products-list/companyAddress";

function Security() {
  return (
    <BaseLayout stickyNavbar>
      <SoftBox mt={5}>
        <Card sx={{ padding: "35px" }}>
          <SoftTypography
            variant="h6"
            mb={2}
            sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
          >
            Business Information
          </SoftTypography>

          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <FormField label="Company Name" placeholder="Company Name" />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormField label="Other Names, IF Any" placeholder="Other Names, IF Any" />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormField label="Network ID" placeholder="Network ID" />
            </Grid>

            <Grid item xs={12} sm={6}>
              <FormField label="Website" placeholder="Website" />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormField
                label="Short Description"
                multiline
                rows={4}
                placeholder="Short Description"
              />
            </Grid>
          </Grid>
        </Card>
        <Card sx={{ padding: "35px", marginTop: "40px" }}>
          <SoftTypography
            variant="h6"
            mb={2}
            sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
          >
            Address
          </SoftTypography>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <SoftBox
                display="flex"
                flexDirection="column"
                justifyContent="flex-end"
                height="100%"
              >
                <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                  <SoftTypography
                    component="label"
                    variant="caption"
                    fontWeight="bold"
                    textTransform="capitalize"
                  >
                    Country
                  </SoftTypography>
                </SoftBox>
                <SoftSelect
                  placeholder="Country"
                  options={[
                    { value: "India", label: "India" },
                    { value: "Asia", label: "Austria" },
                    { value: "USA", label: "USA" },
                  ]}
                />
              </SoftBox>
            </Grid>
            <Grid item xs={12} sm={6}>
              <SoftBox
                display="flex"
                flexDirection="column"
                justifyContent="flex-end"
                height="100%"
              >
                <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                  <SoftTypography
                    component="label"
                    variant="caption"
                    fontWeight="bold"
                    textTransform="capitalize"
                  >
                    State
                  </SoftTypography>
                </SoftBox>
                <SoftSelect
                  placeholder="State"
                  options={[
                    { value: "Tamil Nadu", label: "Tamil Nadu" },
                    { value: "Kerala", label: "Kerala" },
                    { value: "Karnataka", label: "Karnataka" },
                  ]}
                />
              </SoftBox>
            </Grid>
            <Grid item xs={12} sm={6}>
              <SoftBox
                display="flex"
                flexDirection="column"
                justifyContent="flex-end"
                height="100%"
              >
                <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                  <SoftTypography
                    component="label"
                    variant="caption"
                    fontWeight="bold"
                    textTransform="capitalize"
                  >
                    City
                  </SoftTypography>
                </SoftBox>
                <SoftSelect
                  placeholder="City"
                  options={[
                    { value: "Thanjavur", label: "Thanjavur" },
                    { value: "Tiruchirappalli", label: "Tiruchirappalli" },
                    { value: "Madurai", label: "Madurai" },
                  ]}
                />
              </SoftBox>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormField label="Pin" placeholder="Pin" />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormField label="Address" placeholder="Address" multiline rows={4} />
            </Grid>
          </Grid>
        </Card>
        <CompanyAddressList />
        <Divider />
      
      </SoftBox>
    </BaseLayout>
  );
}

export default Security;
