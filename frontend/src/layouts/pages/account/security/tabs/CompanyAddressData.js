
 
const CompanyAddressData = {
  columns: [
    {
      Header: "Address Name",
      accessor: "addressName",
      width: "17%",
    },
    { Header: "Address ID", accessor: "addressID", width: "15%" },
    { Header: "Vat ID", accessor: "vatID", width: "15%" },
    { Header: "Tax ID", accessor: "taxID", width: "15%" },
    { Header: "Address", accessor: "address", width: "15%" },
    { Header: "Country", accessor: "country", width: "15%" },
    { Header: "Profile Status", accessor: "profileStatus", width: "15%" },
    { Header: "Action", accessor: "action" },
  ],
 
  rows: [] ,
};
 
export default CompanyAddressData;
 