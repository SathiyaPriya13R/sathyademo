import React, { useState } from "react";
import { Card, Grid } from "@mui/material";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import FormField from "../../components/FormField";
import SoftButton from "components/SoftButton";
import axios from "axios";

function Contacts() {
  const [formData, setFormData] = useState({
    email: "",
    phone: "",
    fax: "",
    alternativeEmail: "",
    alternativePhone: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async () => {
    try {
      console.log("Submitting data:", formData);
      const response = await axios.post("http://localhost:5000/api/contact/contact", formData);
      console.log(response.data);
      alert("Contact information saved successfully");
    } catch (error) {
      console.error("Error saving contact information:", error);
      alert("Failed to save contact information");
    }
  };

  return (
    <SoftBox mt={5}>
      <Card sx={{ padding: "35px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Company Contact Information
        </SoftTypography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Email"
              placeholder="Email"
              name="email"
              value={formData.email}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Phone"
              placeholder="Phone"
              name="phone"
              value={formData.phone}
              onChange={handleChange}
              inputType="text"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Fax"
              placeholder="Fax"
              name="fax"
              value={formData.fax}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Alternative Email"
              placeholder="Alternative Email"
              name="alternativeEmail"
              value={formData.alternativeEmail}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Alternative Phone"
              placeholder="Alternative Phone"
              name="alternativePhone"
              value={formData.alternativePhone}
              onChange={handleChange}
              inputType="text"
            />
          </Grid>
        </Grid>
        <SoftButton
          variant="gradient"
          color="info"
          sx={{ marginRight: "15px", width: "100px", marginTop: "40px" }}
          onClick={handleSubmit}
        >
          Save
        </SoftButton>
      </Card>
    </SoftBox>
  );
}

export default Contacts;
