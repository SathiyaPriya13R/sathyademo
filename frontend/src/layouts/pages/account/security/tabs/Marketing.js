import React, { useState } from "react";
import { Avatar, Badge, Card, Grid, Stack } from "@mui/material";
import { styled } from "@mui/material/styles";
import PhotoCamera from "@mui/icons-material/PhotoCamera";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import FormField from "../../components/FormField";
import SoftButton from "components/SoftButton";
import axios from 'axios';

function Marketing() {
  const [image, setImage] = useState(null);
  const [formData, setFormData] = useState({
    linkedIn: "",
    instagram: "",
    facebook: "",
    x: "",
    companyDescription: "",
  });

  const handleImageChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onloadend = () => {
      setImage(reader.result);
    };
    if (file) {
      reader.readAsDataURL(file);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async () => {
    try {
      await axios.post("http://localhost:5000/api/marketing/marketing_info", formData);
      console.log("Marketing information saved successfully");
      fetchSavedData(); // Fetch and log saved data after saving
    } catch (error) {
      console.error("Error saving marketing information:", error);
    }
  };

  const fetchSavedData = async () => {
    try {
      const response = await axios.get("http://localhost:5000/api/marketing/get_marketing");
      console.log("Saved marketing information from MongoDB:", response.data);
    } catch (error) {
      console.error("Error fetching marketing information:", error);
    }
  };

  const SmallAvatar = styled(Avatar)(({ theme }) => ({
    width: 22,
    height: 22,
    border: `2px solid ${theme.palette.background.paper}`,
  }));

  const UploadIcon = styled(PhotoCamera)(({ theme }) => ({
    width: 16,
    height: 16,
    color: theme.palette.background.paper,
  }));

  return (
    <SoftBox mt={5}>
      <Card sx={{ padding: "35px" }}>
        <Stack direction="column" alignItems="left" spacing={2}>
          <input
            accept="image/*"
            style={{ display: "none" }}
            id="avatar-upload"
            type="file"
            onChange={handleImageChange}
          />
          <label htmlFor="avatar-upload">
            <Badge
              overlap="circular"
              anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
              badgeContent={
                <SmallAvatar>
                  <UploadIcon />
                </SmallAvatar>
              }
            >
              <Avatar
                alt="User Avatar"
                src={image}
                sx={{ width: 100, height: 100, cursor: "pointer" }}
              />
            </Badge>
          </label>
        </Stack>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Social Networking Links
        </SoftTypography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <FormField
              label="LinkedIn"
              placeholder="LinkedIn"
              name="linkedIn"
              value={formData.linkedIn}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Instagram"
              placeholder="Instagram"
              name="instagram"
              value={formData.instagram}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Facebook"
              placeholder="Facebook"
              name="facebook"
              value={formData.facebook}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="X"
              placeholder="X"
              name="x"
              value={formData.x}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Company Description"
              multiline
              rows={4}
              placeholder="Company Description"
              name="companyDescription"
              value={formData.companyDescription}
              onChange={handleChange}
            />
          </Grid>
        </Grid>
        <SoftButton
          variant="gradient"
          color="info"
          sx={{ marginRight: "15px", width: "100px", marginTop: "40px" }}
          onClick={handleSubmit}
        >
          Save
        </SoftButton>
      </Card>
    </SoftBox>
  );
}

export default Marketing;
