import Card from "@mui/material/Card";
import React, { useState } from "react";
import { 
  Box, 
  Typography,  
  Dialog, 
  DialogContent, 
  TextField, 
  Icon, 
  DialogActions, 
  Button 
} from "@mui/material";
// Assuming Soft UI Dashboard PRO React components and custom components are imported properly
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftButton from "components/SoftButton";
import DataTable from "examples/Tables/DataTable";
// import dataTableReference from "layouts/applications/data-tables/data/dataTableReference";
import ActionCell from "layouts/ecommerce/products/products-list/components/ActionCell";
import { useProfileUIController } from "context";
import { setUpdateProfile } from "context";

function Accounts() {
  const [rows, setRows] = useState([]);
  const [Name, setName] = useState('');
  const [Desgination, setDesgination] = useState('');
  const [Email, setEmail] = useState('');
  const [Phonenumber, setPhonenumber] = useState('');
  const [open, setOpen] = useState(false);
  const [controller, dispatch] = useProfileUIController();



  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleAdd = () => {
    const newRow = {
      name: Name,
      designation: Desgination,
      email: Email,
      phone:Phonenumber,
      action: <ActionCell />,
    };
    setRows([...rows, newRow]);
    setUpdateProfile(dispatch, {"referancedetails": [...rows,newRow]});
    handleClose();
  };

 const columns = [
    { Header: "name", accessor: "name", width: "20%" },
    { Header: "designation", accessor: "designation" },
    { Header: "email", accessor: "email" },
    { Header: "phone", accessor: "phone" },
    { Header: "action", accessor: "action" },
  ];

  return (
    <Card id="reference-details">
      <SoftBox p={3} lineHeight={1}>
        <SoftBox mb={1}>
          <SoftTypography variant="h5">Reference Details</SoftTypography>
        </SoftBox>
      </SoftBox>
  

      {/* <DataTable table={dataTableReference} /> */}
      <DataTable table={{ columns, rows }} />
      <SoftBox ml={{ xs: 0, sm: 5 }} mt={{ xs: 2, sm: 2 }} mb={{ xs: 2, sm: 5 }}>
        <SoftButton variant="gradient" color="info" size="small" onClick={handleClickOpen}>
          <Icon sx={{ fontWeight: "bold" }}>add</Icon>&nbsp; Add New
        </SoftButton>
        <Dialog open={open} onClose={handleClose} maxWidth="md">
          <DialogContent sx={{ width: 500 }}>
            <Box sx={{ display: 'flex', flexDirection: 'column', gap: 3, p: 3 }}>
             
              <Typography variant="h5"  sx={{ textAlign: 'center' }}>Reference Details</Typography>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Typography sx={{ mr: 2.5, minWidth: 120, fontSize: '1rem' }}>Name</Typography>
                <TextField
                  fullWidth
                  placeholder="Name"
                  value={Name}
                  onChange={(e) => setName(e.target.value)}
                />
              </Box>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Typography sx={{ mr: 2.5, minWidth: 120, fontSize: '1rem' }}>Desgination</Typography>
                <TextField
                  fullWidth
                  placeholder="desgination"
                  value={Desgination}
                  onChange={(e) => setDesgination(e.target.value)}
                />
              </Box>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Typography sx={{ mr: 2.5, minWidth: 120, fontSize: '1rem' }}>Email-Id</Typography>
                <TextField
                  fullWidth
                  placeholder="Email-Id"
                  value={Email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </Box>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Typography sx={{ mr: 2.5, minWidth: 120, fontSize: '1rem' }}>Phone number</Typography>
                <TextField
                  fullWidth
                  placeholder="000-000-0000"
                  value={Phonenumber}
                  onChange={(e) => setPhonenumber(e.target.value)}
                />
              </Box>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button onClick={handleAdd}>Add</Button>
          </DialogActions>
        </Dialog>
      </SoftBox>
    </Card>
  );
}

export default Accounts;
