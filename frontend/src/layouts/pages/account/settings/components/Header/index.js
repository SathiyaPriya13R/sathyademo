import React from 'react';
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
import axios from 'axios';
//
// Soft UI Dashboard PRO React components
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftAvatar from "components/SoftAvatar";
import SoftButton from "components/SoftButton";
import { useProfileUIController } from "context"; 
import burceMars from "assets/images/bruce-mars.jpg";

function Header() {
  const [controller, dispatch] = useProfileUIController();

  const Save = async () => {
    console.log(controller);
    try {
      const response = await axios.post('http://localhost:5000/api/candidate/save_candidate', controller);
      console.log('Candidate saved successfully with ID:', response.data.candidate_id);
    } catch (error) {
      console.error('There was an error saving the candidate!', error);
    }
  };

  return (
    <Card id="profile">
      <SoftBox p={2}>
        <Grid container spacing={3} alignItems="center">
          <Grid item>
            <SoftAvatar
              src={burceMars}
              alt="profile-image"
              variant="rounded"
              size="xl"
              shadow="sm"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox height="100%" mt={0.5} lineHeight={1}>
              <SoftTypography variant="h5" fontWeight="medium">
                New Candidate
              </SoftTypography>
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox>
              <SoftButton onClick={Save} variant="gradient" color="info" size="small">
                Save
              </SoftButton>
            </SoftBox>
          </Grid>
        </Grid>
      </SoftBox>
    </Card>
  );
}

export default Header;
