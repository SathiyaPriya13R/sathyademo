/* eslint-disable react/prop-types */
// Soft UI Dashboard React components
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftAvatar from "components/SoftAvatar";
import SoftBadge from "components/SoftBadge";
import EditIcon from "@mui/icons-material/Edit";
// Images
import team2 from "assets/images/team-2.jpg";
import team3 from "assets/images/team-3.jpg";
import team4 from "assets/images/team-4.jpg";
import { IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

function Author({ image, name, email }) {
  return (
    <SoftBox display="flex" alignItems="center" px={1} py={0.5}>
      <SoftBox mr={2}>
        <SoftAvatar src={image} alt={name} size="sm" variant="rounded" />
      </SoftBox>
      <SoftBox display="flex" flexDirection="column">
        <SoftTypography variant="button" fontWeight="medium">
          {name}
        </SoftTypography>
        <SoftTypography variant="caption" color="secondary">
          {email}
        </SoftTypography>
      </SoftBox>
    </SoftBox>
  );
}

function Function({ job, org }) {
  return (
    <SoftBox display="flex" flexDirection="column">
      <SoftTypography variant="caption" fontWeight="medium" color="text">
        {job}
      </SoftTypography>
      <SoftTypography variant="caption" color="secondary">
        {org}
      </SoftTypography>
    </SoftBox>
  );
}

const authorsTableData = {
  columns: [

    { name: "START DATE", align: "left" },
    { name: "END DATE", align: "center" },
    { name: "CREATED DATE", align: "center" },
    { name: "STATUS", align: "center" },
    { name: "ACTIONS", align: "center" },
  ],

  rows: [
    {
      // "START DATE": <Author image={team2} name="John Michael" email="john@creative-tim.com" />,
      // "END DATE": <Function job="Manager" org="Organization" />,
      "START DATE": (
        <SoftTypography variant="caption" color="secondary" fontWeight="medium" sx={{marginLeft:"19px"}}>
          23/04/18
        </SoftTypography>
      ),
      "END DATE": (
        <SoftTypography variant="caption" color="secondary" fontWeight="medium">
          23/04/18
        </SoftTypography>
      ),

      "CREATED DATE": (
        <SoftTypography variant="caption" color="secondary" fontWeight="medium">
          23/04/18
        </SoftTypography>
      ),
      STATUS: (
        <SoftBadge variant="gradient" badgeContent="Active" color="success" size="xs" container />
      ),
      ACTIONS: (
        // <SoftTypography
        //   component="a"
        //   href="#"
        //   variant="caption"
        //   color="secondary"
        //   fontWeight="medium"
        // >
        //   Edit
        // </SoftTypography>
        <div>
      <IconButton aria-label="edit">
        <EditIcon />
      </IconButton>
      <IconButton aria-label="delete">
        <DeleteIcon />
      </IconButton>
    </div>
      ),
    },
    {
      // "START DATE": <Author image={team2} name="John Michael" email="john@creative-tim.com" />,
      // "END DATE": <Function job="Manager" org="Organization" />,
      "START DATE": (
        <SoftTypography variant="caption" color="secondary" fontWeight="medium" sx={{marginLeft:"19px"}}>
          23/04/18
        </SoftTypography>
      ),
      "END DATE": (
        <SoftTypography variant="caption" color="secondary" fontWeight="medium">
          23/04/18
        </SoftTypography>
      ),

      "CREATED DATE": (
        <SoftTypography variant="caption" color="secondary" fontWeight="medium">
          23/04/18
        </SoftTypography>
      ),
      STATUS: (
        <SoftBadge variant="gradient" badgeContent="Inactive" color="Warning" size="xs" container />
      ),
      ACTIONS: (
        // <SoftTypography
        //   component="a"
        //   href="#"
        //   variant="caption"
        //   color="secondary"
        //   fontWeight="medium"
        // >
        //   Edit
        // </SoftTypography>
        <div>
        <IconButton aria-label="edit">
          <EditIcon />
        </IconButton>
        <IconButton aria-label="delete">
          <DeleteIcon />
        </IconButton>
      </div>
      ),
    },
    {
      // "START DATE": <Author image={team2} name="John Michael" email="john@creative-tim.com" />,
      // "END DATE": <Function job="Manager" org="Organization" />,
      "START DATE": (
        <SoftTypography variant="caption" color="secondary" fontWeight="medium" sx={{marginLeft:"19px"}}>
          23/04/18
        </SoftTypography>
      ),
      "END DATE": (
        <SoftTypography variant="caption" color="secondary" fontWeight="medium">
          23/04/18
        </SoftTypography>
      ),

      "CREATED DATE": (
        <SoftTypography variant="caption" color="secondary" fontWeight="medium">
          23/04/18
        </SoftTypography>
      ),
      STATUS: (
        <SoftBadge variant="gradient" badgeContent="online" color="success" size="xs" container />
      ),
      ACTIONS: (
        // <SoftTypography
        //   component="a"
        //   href="#"
        //   variant="caption"
        //   color="secondary"
        //   fontWeight="medium"
        // >
        //   Edit
        // </SoftTypography>
        <div>
      <IconButton aria-label="edit">
        <EditIcon />
      </IconButton>
      <IconButton aria-label="delete">
        <DeleteIcon />
      </IconButton>
    </div>
      ),
    },
  ],
};

export default authorsTableData;
