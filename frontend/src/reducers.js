const initialState = {
    businessInfo: {},
  };
 
  const rootReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'SET_BUSINESS_INFO':
        return {
          ...state,
          businessInfo: action.payload,
        };
      default:
        return state;
    }
  };
 
  export default rootReducer;